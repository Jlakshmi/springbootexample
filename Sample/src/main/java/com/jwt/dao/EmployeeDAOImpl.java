package com.jwt.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jwt.model.Employee;
import com.jwt.model.Question;

@Repository
public  class EmployeeDAOImpl implements EmployeeDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void addEmployee(Employee employee) {
		sessionFactory.getCurrentSession().saveOrUpdate(employee);

	}
	@SuppressWarnings("unchecked")
	public List<Employee> getAllEmployees() {

		return sessionFactory.getCurrentSession().createQuery("from Question").list();
	}

	@SuppressWarnings("unchecked")
	public List<Question> getRightAnswerById(int i) {
		
		return (List<Question>)sessionFactory.getCurrentSession().createQuery("from Question where id=:qid").setParameter("qid",i).list();
	}
	@Override
	@SuppressWarnings("unchecked")
	public List<Question> getAllQuestions() {
	
		return sessionFactory.getCurrentSession().createQuery("from Question").list();
	}
	


	

}