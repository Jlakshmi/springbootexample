package com.jwt.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.jwt.service.EmployeeService;

import com.jwt.model.Employee;
import com.jwt.model.Question;
@Controller
public class EmployeeController
{
	@Autowired
	EmployeeService service;
	@Autowired
	
	@RequestMapping(value="/Login")
	public ModelAndView addEmp(@ModelAttribute("employee")Employee employee)throws IOException{
	return new ModelAndView("EmployeeForm");

}
	@RequestMapping(value="/saveEmployee",method = RequestMethod.POST)
	public ModelAndView saveEmp(@ModelAttribute("employee")@Valid Employee employee,BindingResult res ){
		
		if (res.hasErrors()) {
            return new ModelAndView("EmployeeForm");
        } else 
         service.addEmployee(employee);
         List<Employee> listEmployee = service.getAllEmployees();
         
		return new ModelAndView("quiz1","listEmployee", listEmployee);
	}
	@RequestMapping(value="/result")
	public ModelAndView resultEmp(HttpServletRequest res,HttpServletResponse req,HttpSession session){
		
		int secondpage=(int)session.getAttribute("correctans");	
		int qid=Integer.parseInt(res.getParameter("qno"));
		 List<Employee> listEmployee = service.getAllEmployees();
		for(int i=7;i<=10;i++){
			
			String option1=res.getParameter("option"+i);
			System.out.println("Answer marked"+option1);
			List<Question>list=service.getRightAnswerById(i);
			String correctans2=list.get(0).getRightans();
			if(option1.equals(correctans2)){
				
				secondpage++;	
			}
			session.setAttribute("totalQuestion",10);
			session.setAttribute("correctans",secondpage);
			session.setAttribute("wrongans",10-secondpage);
			}
	    return new ModelAndView("result");
		}
	@RequestMapping(value="/result1")
	public ModelAndView resultEmp2(HttpServletRequest res,HttpSession session ){
		 List<Employee> listEmployee = service.getAllEmployees();
		 int correctans=0;
			
			int qid=Integer.parseInt(res.getParameter("qno"));
			for(int i=1;i<=3;i++){
				
				String option1=res.getParameter("option"+i);
				System.out.println("Answer marked"+option1);
				List<Question>list=service.getRightAnswerById(i);
				String correctans2=list.get(0).getRightans();
				if(option1.equals(correctans2)){
					
				correctans++;	
				}
				session.setAttribute("correctans",correctans);
			
				}
			
         
			return new ModelAndView("quiz2","listEmployee", listEmployee);
	}
	@RequestMapping(value="/result2")
	public ModelAndView resultEmp3(HttpServletRequest res,HttpSession session ){
		 List<Employee> listEmployee = service.getAllEmployees();
		 
			int qid=Integer.parseInt(res.getParameter("qno"));
			int firstpage=(int)session.getAttribute("correctans");	
			for(int i=4;i<=6;i++){
				
				String option1=res.getParameter("option"+i);
				System.out.println("Answer marked"+option1);
				List<Question>list=service.getRightAnswerById(i);
				String correctans2=list.get(0).getRightans();
				if(option1.equals(correctans2)){
					
					firstpage++;	
				}
				session.setAttribute("correctans",firstpage);
			
				}
			
         
			return new ModelAndView("Quiz3","listEmployee", listEmployee);
	}
	@RequestMapping(value="/PDF")
	public ModelAndView pdfConversion(HttpServletRequest res,HttpSession session ){
		 List<Question> listQuestion = service.getAllQuestion();	
		 return new ModelAndView("pdfPage","listQuestion", listQuestion);
	}
}

	