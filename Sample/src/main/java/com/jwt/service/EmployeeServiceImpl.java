package com.jwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jwt.dao.EmployeeDAO;
import com.jwt.model.Employee;
import com.jwt.model.Question;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDAO employeeDAO;
	public void addEmployee(Employee employee) {
		employeeDAO.addEmployee(employee);
	}
	public List<Employee> getAllEmployees() {
		return employeeDAO.getAllEmployees();
	}
	public List<Question> getRightAnswerById(int i){
		return employeeDAO.getRightAnswerById(i);
	}
	public List<Question> getAllQuestion(){
	return employeeDAO.getAllQuestions();
}
}
