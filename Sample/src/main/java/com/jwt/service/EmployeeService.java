package com.jwt.service;

import java.util.List;

import com.jwt.model.Employee;
import com.jwt.model.Question;

public interface EmployeeService {

	public void addEmployee(Employee employee);

	public List<Employee> getAllEmployees();

	public List<Question> getRightAnswerById(int i);

	public List<Question> getAllQuestion();

}
